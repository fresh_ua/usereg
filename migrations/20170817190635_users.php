<?php

use Phinx\Migration\AbstractMigration;

class Users extends AbstractMigration
{

    public function up()
    {
        $users = $this->table('users');
        $users->create();
        $users->addColumn('email', 'string', ['null'=>false])
            ->addColumn('username', 'string', ['null'=>true])
            ->addColumn('password_hash', 'string', ['null'=>false])
            ->addColumn('firstname', 'string', ['null'=>false])
            ->addColumn('lastname', 'string', ['null'=>true])
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'timestamp')
            ->addIndex(['email'], ['unique' => true, 'name' => 'users_email'])
            ->addIndex(['username'], ['unique' => false, 'name' => 'users_username'])
            ->save();
    }

    public function down()
    {
        $this->dropTable('users');
    }
}
