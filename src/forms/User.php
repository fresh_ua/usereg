<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 18.08.17
 * Time: 01:22
 */

namespace App\Forms;
use Psr\Container\ContainerInterface;
use Slim\Container;

class User
{
    public function register($data, $container) {
        $user = new \App\Models\User($container);

        if ($register = $user->setEmail($data['email'])
            ->setPassword($data['password'])
            ->setFirstname($data['firstname'])
            ->setLastname($data['lastname'])
            ->setUsername($data['username'])
            ->save()) {

            $this->login($data, $container);

            return $register;
        }

        return false;
    }

    public function login($data, $container) {
        $user = new \App\Models\User($container);
        $valid = $user->setEmail($data['email'])
            ->valid($data['password']);

        if ($valid) {
            @session_start();
            $_SESSION['login'] = true;
            $_SESSION['name'] = $user->getFullname();
            $_SESSION['id'] = $user->getId();

            return $user->getId();
        }

        return false;
    }
}