<?php
// Routes

use App\Controllers\HomeController;
use App\Controllers\UserController;

$app->get('/', HomeController::class . ':index');
$app->get('/register', UserController::class . ':register');
$app->post('/register', UserController::class . ':registerPost');
$app->get('/login', UserController::class . ':login');
$app->post('/login', UserController::class . ':loginPost');
$app->get('/profile/{id}', UserController::class . ':profile');
$app->get('/logout', UserController::class . ':logout');