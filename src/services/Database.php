<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 18.08.17
 * Time: 09:49
 */

namespace App\Services;


use PDO;
use Psr\Container\ContainerInterface;

class Database
{
    private $pdo;

    public function __construct(ContainerInterface $container, $host, $name, $user, $password)
    {
        if (is_null($this->pdo)) {
            try {
                $this->pdo = new PDO("mysql:host=" . $host . ";dbname=" . $name, $user, $password, [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
            }
            catch (\PDOException $error) {
                /** @var \Monolog\Logger $logger */
                $logger = $container['logger'];
                $logger->addError($error->getMessage());
            }
        }
    }

    public function getPDO()
    {
        return $this->pdo;
    }

}