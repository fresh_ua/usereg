<?php

namespace App\Models;

use PDO;
use Psr\Container\ContainerInterface;

class User
{
    protected $id;
    protected $email;
    private $password_hash;
    protected $username = null;
    protected $firstname;
    protected $lastname = null;

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param mixed $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password_hash = password_hash($password, PASSWORD_BCRYPT);
        return $this;
    }

    /**
     * @return null
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param null $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     * @return $this
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return null
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param null $lastname
     * @return $this
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    public function getFullname()
    {
        $name = $this->getFirstname();
        $lastname = $this->getLastname();
        $username = $this->getUsername();

        if ($lastname)
        {
            $name .= ' '.$lastname;
        }

        if ($username)
        {
            $name .= ' ('.$username.')';
        }

        return $name;
    }

    public function save()
    {
        if ($this->getEmail() && $this->getFirstname() && $this->password_hash) {
            $stm = $this->container['database']->getPDO()->prepare('INSERT INTO users (email, password_hash, username, firstname, lastname)
                                    VALUES (:email, :password, :username, :firstname, :lastname)');
            $result = $stm->execute([
                'email' => $this->getEmail(),
                'password' => $this->password_hash,
                'username' => $this->getUsername(),
                'firstname' => $this->getFirstname(),
                'lastname' => $this->getLastname()
            ]);

            if ($result) {
                return $this->container['database']->getPDO()->lastInsertId();
            }
        }

        return false;
    }

    public function valid($password)
    {
        if ($email = $this->getEmail())
        {
            $stm = $this->container['database']->getPDO()->prepare('SELECT * FROM users WHERE email=:email LIMIT 1');
            if ($stm->execute(['email'=>$email])) {
                $user = $stm->fetchObject();
                if (password_verify($password, $user->password_hash)) {
                    $this->setUsername($user->username)
                        ->setFirstname($user->firstname)
                        ->setLastname($user->lastname)
                        ->setId($user->id);
                    return true;
                }
            }

        }

        return false;
    }

    public static function findByID($container, $id)
    {
        /** @var \PDO $pdo */
        $pdo = $container['database']->getPDO();

        $stm = $pdo->prepare('SELECT * FROM users WHERE id=:user_id LIMIT 1');
        $stm->execute(['user_id'=>$id]);
        return $stm->fetchObject();
    }
    
}