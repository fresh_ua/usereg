<?php

namespace App\Controllers;

class HomeController extends BaseController
{

    public function index($request, $response, $args)
    {
        $this->setTitle('Главная страница');

        return $this->render($response, 'index.twig');
    }

}