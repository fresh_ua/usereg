<?php

namespace App\Controllers;

use App\Forms\User;
use Slim\Exception\NotFoundException;
use Slim\Http\Response;

class UserController extends BaseController
{
    public function register($request, $response, $args)
    {
        $this->setTitle('Регистрация');

        $error = false;
        if (isset($_SESSION['error']))
        {
            $error = $_SESSION['error'];
            unset($_SESSION['error']);
        }

        return $this->render($response, 'register.twig', ['error'=>$error]);
    }

    public function registerPost($request, Response $response, $args)
    {
        $user = new User();
        if ($id = $user->register($request->getParsedBody(), $this->container)) {
            return $response->withRedirect('/profile/'.$id);
        }

        $_SESSION['error'] = 'При регистрации произошла ошибка. Возможно, такой пользователь уже существует.';
        return $this->register($request, $response, $args);
    }

    public function login($request, $response, $args)
    {
        $this->setTitle('Авторизация');

        $error = false;
        if (isset($_SESSION['error']))
        {
            $error = $_SESSION['error'];
            unset($_SESSION['error']);
        }

        return $this->render($response, 'login.twig', ['error'=>$error]);
    }

    public function loginPost($request, Response $response, $args)
    {
        $user = new User();
        if ($id = $user->login($request->getParsedBody(), $this->container)) {
            return $response->withRedirect('/profile/'.$id);
        }

        $_SESSION['error'] = 'Авторизация не удалась. Проверьте логин и пароль';
        return $this->login($request, $response, $args);
    }

    public function profile($request, $response, $args)
    {
        /** @var $route \Slim\Route */
        $route = $request->getAttribute('route');
        $userID = $route->getArgument('id');

        $user = \App\Models\User::findByID($this->container, $userID);
        if ($user) {
            $this->setTitle('Профиль - '.$user->firstname);
            return $this->render($response, 'profile.twig', ['profile' => $user]);
        }

        throw new NotFoundException($request, $response);
    }

    public function logout($request, $response, $args)
    {
        @session_destroy();
        return $response->withRedirect('/');
    }
}