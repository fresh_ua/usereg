<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

class BaseController
{
    protected $container;
    private $title;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title . ' :: Тестовое задание по регистрации пользователей';
    }

    /**
     * @param ResponseInterface $response
     * @param string             $template
     * @param array              $data
     *
     * @return ResponseInterface
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function render(ResponseInterface $response, $template, array $data = [])
    {
        if (isset($this->container['view'])) {
            if (isset($_SESSION['login']) && $_SESSION['login'] === true) {
                $data['username'] = $_SESSION['name'];
                $data['user_id'] = $_SESSION['id'];
            }

            $data['title'] = $this->getTitle();

            return $this->container['view']->render($response, $template, $data);
        }

        return $response;
    }
}