# Тестовое задание

Для реализация этой задачи есть несколько вариантов:

*   Написать все с «чистого листа»
*   Написать с использованием «полноценных» фреймворков
*   Собрать что-то свое на микрофреймворке

Я выбрал последний вариант, т.к. чтобы сделать качественно без каких-либо фреймворков, то это займет очень много времени. Использовал микро-фреймворк Slim3 и Phinx для миграций. Eloquent не подключал специально чтобы показать что есть люди которые знают что такое PDO. Для шаблонизатора выбрал Twig, а для хоть какой-то верстки Bootstrap3\. Автотесты не использовал. Сделано на скорую руку, поэтому для реального использования такое не идет, но для теста, как мне кажется, в самый раз

Тут можно зарегистрироваться, авторизироваться, посмотреть информацию о профиле и выйти из сессии.